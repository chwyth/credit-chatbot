# Credit Chatbot

TransUnion Hackathon 2022 - APAC team

## Getting started

The folder dialog-flow consists of the source code for a chatbot application implemented with [Dialog flow](https://dialogflow.cloud.google.com). The source code for dialog flow's fulfilment (back-end logic) is placed in the dialog-flow folder. It is a Node.js back-end program which can be deployed to Google Cloud Function. The database is [Firestore](https://firebase.google.com).