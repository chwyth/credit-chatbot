// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
"use strict";

const admin = require("firebase-admin");
const functions = require("firebase-functions");
const { Card, WebhookClient } = require("dialogflow-fulfillment");

// Mock up data
const creditCardPlans = require("./mock/credit-card-plans.json");
const personalLoanPlans = require("./mock/personal-loan-plans.json");

admin.initializeApp();
const db = admin.firestore();

const ERROR_WRONG_APPLICATION_TYPE = "Please either choose Credit Card or Personal Loan";

exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  
  // console.debug('Dialogflow Request headers: ' +
  //    JSON.stringify(request.headers));
  // console.debug('Dialogflow Request body: ' + JSON.stringify(request.body));

  /**
   * Handles get offer event.
   * @param {WebhookClient} agent An agent
   */
  function getOfferHandler(agent) {
    const applicantParameters = agent.getContext("loantype-set").parameters;
    console.debug("getOfferHandler parameters: " +
      JSON.stringify(applicantParameters));

    const MAX_OFFER_COUNT = 3;

    const applicationType = applicantParameters["loan-type"];
    if (applicationType == "Credit Card") {
      // Offers are sorted by rebate rate in descending order
      const offers = creditCardPlans.filter(x =>
        applicantParameters["monthly-income"] >= x.minIncome)
        .sort((a, b) => b.rebateRate - a.rebateRate)
        .slice(0, MAX_OFFER_COUNT);
      console.debug("Credit offers: " + JSON.stringify(offers));

      offers.forEach((offer) => {
        const cardTitle = `Offer ${offer.id}: ${offer.provider} 
        ${offer.displayName} ${offer.cardType} ${offer.cardLevel}`;
        const rebateText = "Rebate Rate: " + offer.rebateRate + "%";
        agent.add(new Card({
          title: cardTitle,
          imageUrl: offer.imageUrl,
          text: rebateText
          // buttonText: 'Apply',
          // buttonUrl: 'https://www.google.com.hk'
        }));
      });

      if (offers.length == 0) {
        agent.add("You are unable to apply for a credit card.");
      } else {
        agent.add("Answer 'offer 100' if you want to apply for the offer 100");
      }
    } else if (applicationType == "Personal Loan") {
      // Offers are sorted by APR in ascending order
      const offers = personalLoanPlans.filter((x) =>
        applicantParameters["monthly-income"] >= x.minIncome &&
        applicantParameters.amount <= x.maxAmount)
        .sort((a, b) => a.apr - b.apr).slice(0, MAX_OFFER_COUNT);
      console.debug("Loan offers: " + JSON.stringify(offers));

      offers.forEach((offer) => {
        const aprText = `APR: ${offer.apr}%, Maximum repayment period: 
        ${offer.maxTenor} months`;
        agent.add(new Card({
          title: `Offer ${offer.id}: ${offer.provider}`,
          imageUrl: offer.imageUrl,
          text: aprText
          // buttonText: 'Apply',
          // buttonUrl: 'https://www.google.com.hk'
        }));
      });

      if (offers.length == 0) {
        agent.add("You are unable to apply for a personal loan.");
      } else {
        agent.add("Answer 'offer 100' if you want to apply for the offer 100");
      }
    } else {
      agent.add(ERROR_WRONG_APPLICATION_TYPE);
      console.log("Wrong application type");
    }
  }

  /**
   * Handles send offer event, i.e. sends an application to the credit card
   * or loan provider.
   * @param {WebhookClient} agent
   */
  async function sendOfferHandler(agent) {
    const applicantParameters = agent.getContext("loantype-set").parameters;
    // console.debug("sendOfferHandler parameters: " +
    //   JSON.stringify(applicantParameters));
    const applicationType = applicantParameters["loan-type"];

    // Remove preceding 'offer'
    const selectedOfferId = applicantParameters["selected-offer"]
      .toLowerCase().replace("offer ", "");

    let applicationRecord = { "applicationType": applicationType };

    if (applicationType == "Credit Card") {
      let matchedOffer = creditCardPlans.find(offer => offer.id == selectedOfferId);
      if (matchedOffer) {
        applicationRecord.offer = {
          "id": matchedOffer.id,
          "provider": matchedOffer.provider
        }
      }
      applicationRecord.product = `${matchedOffer.cardType} ${applicantParameters["card-level"]}`;
      applicationRecord.displayName = matchedOffer.displayName;
    } else if (applicationType == "Personal Loan") {
      let matchedOffer = personalLoanPlans.find(offer => offer.id == selectedOfferId);
      if (matchedOffer) {
        applicationRecord.offer = {
          "id": matchedOffer.id,
          "provider": matchedOffer.provider
        }
      }
      applicationRecord.amount = applicantParameters.amount;
      applicationRecord.tenor = applicantParameters.tenor;
    } else {
      agent.add(ERROR_WRONG_APPLICATION_TYPE);
    }

    if (applicationRecord.offer.id == -1) {
      agent.add(`Offer ${selectedOfferId} is unavailable.
      Answer 'offer 100' if you want to apply for the offer 100.`);
    } else {
      applicationRecord.address = applicantParameters.address;
      applicationRecord.firstName = applicantParameters["given-name"];
      applicationRecord.lastName = applicantParameters["last-name"];
      applicationRecord.idNumber = applicantParameters["id-num"];
      applicationRecord.phoneNumber = applicantParameters["phone-number"];
      applicationRecord.monthlyIncome = applicantParameters["monthly-income"];
      applicationRecord.status = "Submitted";
      applicationRecord.createdAt = admin.firestore.Timestamp.now();

      // console.debug("applicationRecord: " + JSON.stringify(applicationRecord));
      console.log(`selectedOfferId=${selectedOfferId}`);

      const document = await db.collection("application-record").add(applicationRecord);
      console.log(`Application record ${document.id} is added to the database.`);
      agent.add(`You have applied for offer ${selectedOfferId}. Ref Num: ${document.id}`);
    }
  }

  async function findApplicationRecordHandler(agent) {
    let refNum = agent.parameters["ref-num"];
    console.log(`Finding the application record ${refNum}`);
    const document = await db.collection("application-record").doc(refNum).get();
    if (!document.exists) {
      console.log(`${refNum} is not found`);
      agent.add(`The reference number ${refNum} is not found.`);
    } else {
      console.log(`${refNum} is found`);

      let extraMessage = "";
      if (document.data().applicationType == "Credit Card") {
        extraMessage =
        `Required Card: ${document.data().displayName} ${document.data().product}`;
      } else if (document.data().applicationType == "Personal Loan") {
        extraMessage =
        `Amount: ${document.data().amount}
        Repayment period: ${document.data().tenor.amount} ${document.data().tenor.unit}`;
      }

      agent.add(`Applicant: ${document.data().firstName} ${document.data().lastName}
      Applied Product: ${document.data().applicationType}
      Provider: ${document.data().offer.provider}
      Status: ${document.data().status}
      ${extraMessage}`);
    }
  }

  // Run the proper function handler based on the matched Dialogflow intent name
  const intentMap = new Map();
  intentMap.set("Common2 Contacts", getOfferHandler);
  intentMap.set("Send Application Intent", sendOfferHandler);
  intentMap.set("Find Application Record Intent", findApplicationRecordHandler)
  agent.handleRequest(intentMap);
});